/*
 *************************************************************************
 **                           LOCATIONS SCRIPT                          **
 *************************************************************************
*/

$(document).ready(function () {

    svgPOISetUsedItems();
    svgPOIClickHandler();
    labelPOIClickHandler();
    elementCloseClickHandler();

    var onShowOnFirst = false;

    // On POI-Select change
    $('.tx-tt3-poi select.poi--select').change( function () {
        var val = $(this).val();
        var parent = $(this).find(':selected').attr('data-parent');

        //console.log("onChange parent: " + parent + " val:" + val);

        if ( isNotNullOrEmpty(val) && isNotNullOrEmpty(parent) ) {
            poiChange(val, parent, onShowOnFirst);
        } else {
            $(this).val($.data(this, 'current'));
            return false;
        }

        $.data(this, 'current', $(this).val());
        onShowOnFirst = false;

    });

    // Select ShowOnFirst
    $(".tx-tt3-poi select.poi--select option[data-showonfirst='1']").each(function() {

        var val = $(this).val();
        var parent = $(this).attr('data-parent');

        //console.log("onFirst parent: " + parent + " val:" + val);

        onShowOnFirst = true;
        $('.tx-tt3-poi #poi-' + parent + '-select').val(val).change();

    });

});//end Document ready



/*
 * add class for in use svg items
 */
function svgPOISetUsedItems() {

    $('.tx-tt3-poi select.poi--select option').each(function() {
        var $svgObjectId = $(this).val();
        var $svgObjectLabel = $(this).text();

        if( $svgObjectId != ""){
            $('.poigraphic-container svg #' + $svgObjectId).addClass("inUse");

            if( $svgObjectLabel != ""){
                $('.poigraphic-container svg #' + $svgObjectId).attr('title', $svgObjectLabel);
            }
        }

    });
}


/*
 * set active on svg click
 */
function svgPOIClickHandler() {
    $('.tx-tt3-poi .poigraphic-container svg .poi').unbind('click').bind('click', function (e) {
        var $active_poielementsvgid = $(this).attr("id");
        //set POI Select Value
        $('.tx-tt3-poi select.poi--select').val($active_poielementsvgid).change();
    });
}

/*
* set active on label click
*/
function labelPOIClickHandler() {
    $('.tx-tt3-poi .poi--label').unbind('click').bind('click', function (e) {
        var $active_poielementsvgid = $(this).attr("data-poielement-svgid");
        //set POI Select Value
        $('.tx-tt3-poi select.poi--select').val($active_poielementsvgid).change();
    });
}

/*
* close element on click
*/
function elementCloseClickHandler() {
    $('.tx-tt3-poi .poi--element .btn-close').unbind('click').bind('click', function (e) {
        var parent = $(this).parent().attr('data-parent');
        poiChange("",parent,true);
    });
}

/*
 * POI (Select) change
 */
function poiChange( $svgid, $parent, $onShowOnFirst ) {

    if ($svgid != "") {

        //POI--list
        $('.poi--element[data-parent="' + $parent + '"]').fadeOut(400);
        $('.poi--element[data-parent="' + $parent + '"]').fadeOut(400);
        $('.poi--element[data-parent="' + $parent + '"]').removeClass("active");

        $('*[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').delay(400).fadeIn(400);
        $('*[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').addClass("active");

        //svg-graphic
        $('#poi-' + $parent + '-graphic svg .poi').removeClass("active");
        $('#poi-' + $parent + '-graphic svg #' + $svgid).addClass("active");

        //label buttons
        $('.poi--label[data-parent="' + $parent + '"]').removeClass("active");
        $('.poi--label[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').addClass("active");

        //check if linked
        var link = $('.poi--element[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').attr("data-poielement-link");
        var uri = $('.poi--element[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').attr("data-poielement-link-uri");
        if(link != "" && uri != "") {
            var target = link.split(' ')[1];
            if(typeof target == 'undefined' || target == 'undefined' || target == '') {
                target = "_self";
            }
            window.open(uri, target);
        }

    } else {

        //POI--list
        $('.poi--element[data-parent="' + $parent + '"]').fadeOut(400);
        $('.poi--element[data-parent="' + $parent + '"]').removeClass("active");

        //svg-graphic
        $('#poi-' + $parent + '-graphic svg .poi').removeClass("active");

        //label buttons
        $('.poi--label[data-parent="' + $parent + '"]').removeClass("active");

    }

    $(document).trigger("poiChangeTriggered");

    // scroll to results
    if(!$onShowOnFirst){
        scrollToResult(link, uri, $parent);
    }

}

function scrollToResult($link, $uri, $parent) {

    var doScrollToResult = $('.tx-tt3-poi').attr('data-scroll-to-result');
    if(doScrollToResult) {
        const scrollToResultMaxWidth = -1;
        const mq = window.matchMedia( "(max-width: " + scrollToResultMaxWidth + "px)" );

        if (scrollToResultMaxWidth <= 0 || mq.matches) {
            var scrollTarget = "";

            if($link != "" && $uri != "") {
                scrollTarget = $("a[name=" +  $uri.substr($uri.indexOf('#')+1) + "]");
            } else {
                scrollTarget = $("a[name=poi-" + $parent + "-results]");
                if(
                    scrollTarget.length && scrollTarget != "" && scrollTarget != "undefined"
                    && !$("body").hasClass("anchorscrolling-off")
                ){
                    $('html, body').animate({scrollTop: scrollTarget.offset().top + 'px'}, 600);
                }
            }
        }
    }
}

function isNotNullOrEmpty (val) {
    switch (val) {
        case "":
            return false;
        case 0:
            return false;
        case null:
            return false;
        case false:
            return false;
        case undefined:
            return false;
        case typeof this === 'undefined':
            return false;
        default: return true;
    }
}
