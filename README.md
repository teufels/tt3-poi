[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--poi-orange.svg)](https://bitbucket.org/teufels/tt3-poi/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__poi-red.svg)](https://bitbucket.org/teufels/tt3-poi/src/main/)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)

[ ṯeufels ] Point of Interest
==========
Point of Interest Elements on one SVG-Graphic

#### This version supports TYPO3
![SUPPORTS](https://img.shields.io/badge/12_LTS-%23A3C49B.svg?style=flat-square)
![SUPPORTS](https://img.shields.io/badge/13_LTS-%23A3C49B.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-poi`

***

### Requirements
`teufels/tt3-image`

***

### SVG Requirements
* objects to use must have a unique id and the class "poi"

***

### How to Use
1. Install with composer
2. Import Static Template (before sitepackage)
3. Get SVG Graphic with objects, which should be used, with a unique id and the class "poi"  
![INFO](https://img.shields.io/badge/info-SVG%20Object%20that%20are%20used%20poi%20element%20with%20corresponding%20id%20exist%20get%20the%20class%20inUse%20and%20become%20clickable-lightgrey?style=flat-square)  
![WARNING](https://img.shields.io/badge/warning-if%20used%20two%20or%20more%20POI--CE%20on%20same%20page%20be%20sure%20that%20SVG%20Object%20id%20is%20unique%20across%20all-red?style=flat-square)
4. Override Template (optional)
   1. Make your adjustments to Template  
   ![INFO](https://img.shields.io/badge/info-By%20default%20usage%20of%20Grid%20with%20Graphic%20right%20and%20InfoWindow%20left-lightgrey?style=flat-square)
   2. Make your adjustments to Styling  
   ![INFO](https://img.shields.io/badge/info-Style%20stroke%2C%20fill%2C%20opacity%20for%20.poigraphic--container%20svg%20.poi%20general%2C%20%3Ahover%2C%20.active-lightgrey?style=flat-square)

***

### Update & Migration from hive_poi
1. in composer.json replace `beewilly/hive_poi` with `"teufels/tt3-poi":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Point of Interest`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_poi yet)
5. Perform Upgrade Wizards `[teufels] Point of Interest`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_poi now)
7. class & id changed -> adjust styling in sitepackage (e.g. tx-hive-poi => tx-tt3-poi)
8. check & adjust be user group access rights

***

### Code Examples

###### Override Template
```
plugin.tx_tt3poi {
    view {
        templateRootPaths >
        templateRootPaths {
            0 = EXT:tt3_poi/Resources/Private/Templates/
            10 = EXT:sitepackage/Resources/Private/Overrides/tt3_poi/Templates/
        }
    }
}
```

***

### Changelog
#### 1.1.x
- 1.1.0 add support for TYPO3 v13
#### 1.0.x
- 1.0.11 add Trigger Event "poiChangeTriggered"
- 1.0.10 change Plugin Title
- 1.0.9 fix renderContentElementPreviewFromFluidTemplate changed parameters
- 1.0.7 - 1.0.8 eval rules for cutomclass
- 1.0.6 add close button to POI Element & option to disable scrolling to result by data-attribute
- 1.0.5 change c-<id> to c<id> & improve BE Preview
- 1.0.4 outsource "scroll to Result" in own function to make it easier to overwrite and add query condition
- 1.0.3 fix not toggle active for poi--label
- 1.0.2 changed deprecated allowTableOnStandardPages() to ignorePageTypeRestriction (https://docs.typo3.org/m/typo3/reference-tca/12.4/en-us/Ctrl/Properties/Security.html#ctrl-security-ignorepagetyperestriction)
- 1.0.1 fix undefined Array Key error
- 1.0.0 intial from [hive_poi](https://bitbucket.org/teufels/hive_poi/src/)