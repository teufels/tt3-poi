<?php
declare(strict_types=1);

namespace Teufels\Tt3Poi\EventListener;

use \TYPO3\CMS\Backend\View\Event\IsContentUsedOnPageLayoutEvent as OriginalIsContentUsedOnPageLayoutEvent;

class IsContentUsedOnPageLayoutEvent {
    public function __invoke(OriginalIsContentUsedOnPageLayoutEvent $event): void {

        // Hide Child Content Element at module "Page" at the backend
        if($event->isRecordUsed() === false && $event->getRecord()['CType'] !== 'tt3poi_tt3_poi' && $event->getRecord()['colPos'] === 999) {
            $event->setUsed(true);
        }
    }
}
