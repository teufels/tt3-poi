<?php
declare(strict_types=1);

namespace Teufels\Tt3Poi\EventListener;

use TYPO3\CMS\Core\Database\Connection;
use \TYPO3\CMS\Backend\View\Event\ModifyDatabaseQueryForRecordListingEvent;

/**
 * Event for PageLayoutView to hide tt_content elements in page view
 */
final class ModifyDatabaseQueryForRecordListingEventListener {

    public function modify(ModifyDatabaseQueryForRecordListingEvent $event): void {
        if ($event->getTable() === 'tt_content' && $event->getPageId() > 0) {
            // Only hide elements which are inline, allowing for standard
            // elements to show
            $event->getQueryBuilder()->andWhere(
                $event->getQueryBuilder()->expr()->lte('tx_tt3poi_element_content_parent', $event->getQueryBuilder()->createNamedParameter(0, Connection::PARAM_INT))
            );
        }
    }
}
