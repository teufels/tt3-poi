<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_poi" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Poi\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3poiDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Point of Interest: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hivepoi_poielement to the new tx_tt3poi_element';
        if($this->checkMigrationTableExist()) { $description .= ': ' . count($this->getMigrationRecords()); }
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist();
    }

    public function performMigration(): bool
    {
        //data
        $sql = "INSERT INTO tx_tt3poi_element(uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,element_customcssclasses,element_content,element_label,element_link,element_svgid,element_showonfirst) 
        SELECT uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,tx_hivepoi_poielcustomcssclasses,tx_hivepoi_poielementcontent,tx_hivepoi_poielementlabel,tx_hivepoi_poielementlink,tx_hivepoi_poielementsvgid,tx_hivepoi_poielementshowonfirst
        FROM tx_hivepoi_poielement WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3poi_element');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        //sys_file_reference
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('sys_file_reference');
        $queryBuilder
            ->update('sys_file_reference')
            ->set('fieldname', 'tx_tt3poi_graphic')
            ->where(
                $queryBuilder->expr()->eq('fieldname', $queryBuilder->createNamedParameter('tx_hivepoi_poigraphic'))
            )
            ->executeStatement();

        //Nested CE
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder
            ->update('tt_content')
            ->set('tx_tt3poi_element_content_parent',$queryBuilder->quoteIdentifier('tx_hivepoi_poielementcontent_parent'),false)
            ->where(
                $queryBuilder->expr()->gt('tx_hivepoi_poielementcontent_parent', 0)
            )
            ->executeStatement();

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_hivepoi_poielement');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from('tx_hivepoi_poielement')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_hivepoi_poielement')
            ->createSchemaManager()
            ->tablesExist(['tx_hivepoi_poielement']);
    }
}
