<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_poi" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Poi\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3poiPluginUpdater')]
class PluginUpdater implements UpgradeWizardInterface
{
    private const SOURCE_CTYPE = 'hivepoi_hive_poi';
    private const TARGET_CTYPE = 'tt3poi_tt3_poi';

    public function getTitle(): string
    {
        return '[teufels] Point of Interest: Migrate plugin';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all existing plugin to new one. Count of plugins: ' . count($this->getMigrationRecords());
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return count($this->getMigrationRecords()) > 0;
    }

    public function performMigration(): bool
    {
        $records = $this->getMigrationRecords();

        foreach ($records as $record) {
            $this->updateContentElement($record['uid'], self::TARGET_CTYPE);
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid', 'CType')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq(
                    'CType',
                    $queryBuilder->createNamedParameter(self::SOURCE_CTYPE)
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }


    /**
     * Updates Ctype and plugindata of the given content element UID
     *
     * @param int $uid
     * @param string $newCtype
     */
    protected function updateContentElement(int $uid, string $newCtype): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
        $queryBuilder->update('tt_content')
            ->set('CType', $newCtype)
            ->set('tx_tt3poi_graphic',$queryBuilder->quoteIdentifier('tx_hivepoi_poigraphic'),false)
            ->set('tx_tt3poi_removesvgstyleattributes',$queryBuilder->quoteIdentifier('tx_hivepoi_poiremovesvgstyleattributes'),false)
            ->set('tx_tt3poi_customcssclasses',$queryBuilder->quoteIdentifier('tx_hivepoi_poicustomcssclasses'),false)
            ->set('tx_tt3poi_element',$queryBuilder->quoteIdentifier('tx_hivepoi_poielement'),false)
            ->set('tx_tt3poi_element_content_parent',$queryBuilder->quoteIdentifier('tx_hivepoi_poielementcontent_parent'),false)
            ->where(
                $queryBuilder->expr()->in(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, Connection::PARAM_INT)
                )
            )
            ->executeStatement();
    }

}
