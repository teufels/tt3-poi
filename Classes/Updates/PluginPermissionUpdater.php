<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_poi" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Poi\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3poiPluginPermissionUpdater')]
class PluginPermissionUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Point of Interest: Migrate plugin permissions';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard updates all permissions and allows tt3poi_tt3_poi plugin instead of the previous hivepoi_hive_poi plugin.';
        $description .= ' Count of affected groups: ' . count($this->getMigrationRecords());
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return count($this->getMigrationRecords()) > 0;
    }

    public function performMigration(): bool
    {
        $records = $this->getMigrationRecords();

        foreach ($records as $record) {
            $this->updateRow($record);
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('be_groups');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid', 'non_exclude_fields', 'explicit_allowdeny', 'tables_select', 'tables_modify')
            ->from('be_groups')
            ->where(
                $queryBuilder->expr()->like(
                    'explicit_allowdeny',
                    $queryBuilder->createNamedParameter('%' . $queryBuilder->escapeLikeWildcards('tt_content:CType:hivepoi_hive_poi') . '%')
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function updateRow(array $row): void
    {

        $old = 'tt_content:CType:hivepoi_hive_poi';
        $new = 'tt_content:CType:tt3poi_tt3_poi';
        $newListExplicitAllowdeny = str_replace($old, $new, $row['explicit_allowdeny']);

        $old = 'tx_hivepoi_poielement';
        $new = 'tx_tt3poi_element';
        $newListTablesSelect = str_replace($old, $new, $row['tables_select']);
        $newListTablesModify = str_replace($old, $new, $row['tables_modify']);
        $newListNonExcludeFields = str_replace($old, $new, $row['non_exclude_fields']);

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('be_groups');
        $queryBuilder->update('be_groups')
            ->set('non_exclude_fields', $newListNonExcludeFields)
            ->set('explicit_allowdeny', $newListExplicitAllowdeny)
            ->set('tables_select', $newListTablesSelect)
            ->set('tables_modify', $newListTablesModify)
            ->where(
                $queryBuilder->expr()->in(
                    'uid',
                    $queryBuilder->createNamedParameter($row['uid'], Connection::PARAM_INT)
                )
            )
            ->executeStatement();
    }
}
