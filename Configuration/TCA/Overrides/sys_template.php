<?php
defined('TYPO3') or die();

$extensionKey = 'tt3_poi';
$extensionTitle = '[teufels] Point of Interest';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);