CREATE TABLE tt_content (
    backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_tt3poi_graphic int(11) unsigned DEFAULT '0' NOT NULL,
    tx_tt3poi_removesvgstyleattributes tinyint(4) DEFAULT '0' NOT NULL,
    tx_tt3poi_customcssclasses tinytext,
    tx_tt3poi_element int(11) unsigned DEFAULT '0' NOT NULL,
    tx_tt3poi_element_content_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_tt3poi_element_content_parent (tx_tt3poi_element_content_parent,pid,deleted)
);
CREATE TABLE tx_tt3poi_element (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    element_customcssclasses tinytext,
    element_content int(11) unsigned DEFAULT '0' NOT NULL,
    element_label tinytext,
    element_link tinytext,
    element_svgid tinytext,
    element_showonfirst tinyint(4) DEFAULT '0' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
